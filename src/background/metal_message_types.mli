(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Js

(* Message types *)

class type site_metadata = object
  method name : js_string t readonly_prop
  method icon : js_string t optdef readonly_prop
  method url : js_string t readonly_prop
end

class type ['a] request = object
  method id : js_string t readonly_prop
  method name : js_string t readonly_prop
  method data : 'a optdef readonly_prop
  method metadata : site_metadata t optdef prop
end

class type ['a] message = object
  method src : js_string t readonly_prop
  method req : 'a request t readonly_prop
end

class type ['a] response = object
  method id : js_string t readonly_prop
  method ok : bool t readonly_prop
  method result : 'a readonly_prop
end

(* inpage.js receives only answer and state_change objects *)

class type ['a] answer = object
  method src : js_string t readonly_prop
  method res : 'a response t readonly_prop
end

class type ['a] state_change = object
  method src : js_string t readonly_prop
  method state_change_ : 'a t readonly_prop
end

(* callback notif types *)

class type ['a] notif_callback_msg = object
  method ok : bool t readonly_prop
  method msg : 'a t optdef readonly_prop
end

class type ['a] commit_callback_msg = object
  method wid : int readonly_prop
  method commit_result_ :'a notif_callback_msg t readonly_prop
end

class type postpone = object
  method time : int readonly_prop
  method wid : int readonly_prop
end

class type add_to_batch = object
  inherit postpone
  method notif : Storage_types.Local.notif_js t readonly_prop
end

(* request types *)
type manager_op = Storage_types.Local.notif_manager_info_js t Storage_types.Local.manager_operation_js

class type position = object
  method x : int readonly_prop
  method y : int readonly_prop
end

class type config = object
  method position : position t optdef readonly_prop
  method network : js_string t optdef readonly_prop
end

class type op_config = object
  inherit config
  method type_ : js_string t optdef readonly_prop
end

class type op = object
  inherit op_config
  method op : manager_op t readonly_prop
end

class type batch = object
  inherit op_config
  method ops : manager_op t js_array t readonly_prop
end

class type rpc = object
  inherit config
  method rpc : js_string t optdef readonly_prop
end
