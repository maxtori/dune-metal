open Ezjs_min
open Storage_utils
open Vue_component

let pp_amount app =
  let precision = Of_js.optdef_exn (to_optdef Of_js.to_int) app##.precision in
  let width = Of_js.optdef_exn (to_optdef Of_js.to_int) app##.width in
  let order = Of_js.optdef_exn (to_optdef Of_js.to_int) app##.order in
  try match to_optdef Of_js.to_int64 app##.amount with
    | None -> "--", -1, None, _true
    | Some amount ->
      let int_part, order, dec_part =
        Dun.pp_amount0 ?precision ?width ?order amount in
      int_part, order, dec_part, _false
  with _ ->
  match Of_js.optdef_exn (to_optdef (fun x -> Of_js.amount x (def (string "dun")))) app##.amount with
  | None -> "--", -1, None, _true
  | Some amount ->
    let int_part, order, dec_part =
      Dun.pp_amount0 ?precision ?width ?order amount in
    int_part, order, dec_part, _false

let v_dun () =
  make
    ~render:Render.dun_render
    ~static_renders:Render.dun_static_renders
    ~props:(PrsArray [ "precision"; "width"; "order"; "symbol"; "amount";
                       "unit_class"; "decimal_class"; "dun_class"; "undef_title";
                       "undef_class" ])
    ~data:(fun app ->
        object%js
          val dun_cl_ = unoptdef (string "pp-dun") (Unsafe.coerce app)##.dun_class_;
          val unit_cl_ = unoptdef (string "pp-dun-unit") (Unsafe.coerce app)##.unit_class_;
          val dec_cl_ = unoptdef (string "pp-dun-decimal") (Unsafe.coerce app)##.decimal_class_;
        end)
    ~computed:(Mjs.L [
        "pp", fun app ->
          let int_part, order, dec_part, undefined_amount = pp_amount app in
          def @@ Mjs.to_any @@ object%js
            val int = string int_part
            val dec = optdef string dec_part
            val icon = string @@ Dun.symbol_str ~order ()
            val undef = undefined_amount
          end])
    "v-dun"

let v_account () =
  make
    ~render:Render.account_render
    ~static_renders:Render.account_static_renders
    ~props:(PrsArray ["pkh"; "size"; "scale"; "blockies_class"; "hash_class"; "show_blockies";
                      "show_hash"; "len"; "height"; "net"; "ratio"])
    ~computed:(Mjs.L [
        "pr", fun app ->
          let height = unoptdef 15 app##.height in
          let ratio = unoptdef_f 1.5 to_float app##.ratio in
          let path = match Optdef.to_option app##.net with
            | Some net ->
              let network = Mhelpers.network_of_string @@ to_string net in
              def @@ string @@ Mhelpers.dunscan_path network ^ (to_string app##.pkh)
            | None -> undefined in
          let x = object%js
            val size_bl_ = unoptdef 8 app##.size
            val scale_bl_ = unoptdef 8 app##.scale
            val blockies_cl_ = unoptdef (string "blockies-img") app##.blockies_class_
            val hash_cl_ = unoptdef (string "blockies-hash") app##.hash_class_
            val sh_blockies_ = unoptdef _true app##.show_blockies_
            val sh_hash_ = unoptdef _true app##.show_hash_
            val length = unoptdef 10 app##.len
            val w_blockies_ = int_of_float (float_of_int height *. ratio)
            val w_hash_ = height
            val path = path
          end in
          def @@ Mjs.to_any x])
    "v-account"

let v_amount () =
  make
    ~render:Render.amount_render
    ~static_renders:Render.amount_static_renders
    ~props:(PrsArray [ "amount"; "burn"; "kind"; "collectCall"; "managerKT"; "fee"; "total" ])
    "v-amount"

let v_transaction () =
  make
    ~render:Render.transaction_render
    ~static_renders:Render.transaction_static_renders
    ~props:(PrsArray [ "op"; "clear_op"; "show_op"; "route"; "expandable"; "expanded" ])
    ~data:(fun _ -> object%js val postpone = _false end)
    "v-transaction"

let v_delegation () =
  make
    ~render:Render.delegation_render
    ~static_renders:Render.delegation_static_renders
    ~props:(PrsArray [ "core"; "clear_op"; "show_op"; "route"; "expandable"; "expanded" ])
    "v-delegation"

let v_manage_account () =
  make
    ~render:Render.manage_account_render
    ~static_renders:Render.manage_account_static_renders
    ~props:(PrsArray [ "core"; "clear_op"; "show_op"; "set_manage_account_json" ])
    "v-manage-account"

let v_notif () =
  make
    ~render:Render.notif_render
    ~static_renders:Render.notif_static_renders
    ~props:(PrsArray [ "core"; "close_notif"; "refuse_notif"; "inject_op"; "approve_url";
                       "dune_script"; "add_to_batch"; "postpone_batch" ])
    "v-notif"

let v_notif_small () =
  make
    ~render:Render.notif_small_render
    ~static_renders:Render.notif_small_static_renders
    ~props:(PrsArray [ "notif"; "network"; "show_notif"; "deny_notif" ])
    "v-notif-small"

let v_import_kt1 () =
  make
    ~render:Render.import_kt1_render
    ~static_renders:Render.import_kt1_static_renders
    ~props:(PrsArray [ "core"; "import_accounts"; "clear_import" ])
    "v-import-kt1"

let load () =
  Blockies.init ();
  ignore @@ v_dun ();
  ignore @@ v_account ();
  ignore @@ v_transaction ();
  ignore @@ v_delegation ();
  ignore @@ v_manage_account ();
  ignore @@ v_amount ();
  ignore @@ v_notif ();
  ignore @@ v_notif_small ();
  ignore @@ v_import_kt1 ()
