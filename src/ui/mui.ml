(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min

let days d = if d > 1 then "days" else "day"

let parse_seconds t =
  if t < 60 then
    Printf.sprintf "%02d s" t
  else
    if t < 3600 then
      let secs = t mod 60 in
      let mins = t / 60 in
      if t < 600 then
        Printf.sprintf "%dm %02ds" mins secs
      else
        Printf.sprintf "%d min" mins
    else
      let t = t / 60 in
      if t < 60 * 24 then
        let mins = t mod 60 in
        let hours = t / 60 in
        Printf.sprintf "%dh %dm" hours mins
      else
        let t = t / 60 in
        if t < 72 then
          let hours = t mod 24 in
          let ds = t / 24 in
          Printf.sprintf "%d %s %dh" ds (days ds) hours
        else
          let ds = t / 24 in
          Printf.sprintf "%d %s" ds (days ds)

let get_now () = (new%js date_now)##valueOf

let ago timestamp_f =
  (get_now () -. timestamp_f) /. 1000.

let ago_str ?(future=false) timestamp_f =
  let diff = int_of_float @@ ago timestamp_f in
  parse_seconds @@ if future then -1 * diff else diff

let float_of_iso timestamp =
  date##parse (string timestamp)

let print_script ?contract s =
  match Dune.parse_script ?contract s with
  | Error _ -> s
  | Ok sc -> Dune.print_script ?contract sc

module IntMap = Map.Make(Int)
module Timer = struct

  let timer_id = ref 0
  let timers = ref (IntMap.empty : (string option * Dom_html.interval_id) IntMap.t)

  let get i = IntMap.find_opt i !timers

  let remove ?name i (na, timer) =
    match name with
    | Some name when Some name <> na -> ()
    | _ ->
      timers := IntMap.remove i !timers;
      Dom_html.window##clearInterval timer

  let clear ?name i = match get i with
    | None -> ()
    | Some x -> remove ?name i x

  let clear_timers ?name () =
    IntMap.iter (remove ?name) !timers

  let create ?name time_s f =
    incr timer_id;
    let id = !timer_id in
    f ();
    let cb () =
      Chrome.Tabs.getCurrent (fun t ->
          if to_bool t##.active then f ()) in
    let timer = Dom_html.window##setInterval
        (wrap_callback cb)
        (float_of_int time_s *. 1000.) in
    timers := IntMap.add id (name, timer) !timers

  let clear_create ?name time_s f =
    clear_timers ?name ();
    create ?name time_s f
end

let set_timeout f time =
  Dom_html.window##setTimeout (wrap_callback f) time

let wait ?(t=1.) f = set_timeout f t |> ignore

let make_error code content =
  object%js
    val code = code
    val content = optdef string content
  end

let read_file ?(error=(fun () -> ())) file f =
  let reader = new%js File.fileReader in
  reader##.onloadend :=
    Dom.handler (fun _evt ->
        if reader##.readyState = File.DONE then
          Opt.case (File.CoerceTo.string (reader##.result)) error
            (fun s -> f (to_string s));
        _true);
  reader##(readAsBinaryString file)

let input_by_chunk_file ?(chunk_size=1048576) file process finalize =
  let size = file##.size in
  let name = to_string file##.name in
  let reader = new%js File.fileReader in
  let start = ref 0 in
  let aux () =
    let blob = file##slice !start (!start + chunk_size) in
    reader##readAsArrayBuffer blob in
  reader##.onloadend :=
    Dom.handler (fun _evt ->
        if reader##.readyState = File.DONE then (
          process (reader##.result);
          start := !start + chunk_size;
          if !start < size then aux ()
          else finalize name);
        _true);
  aux ()

(* for account history *)
