module Compare = Compare

module Signature = struct

  module Public_key = struct
    type t = string
    let compare = String.compare
    let encoding = (Binary_writer.pk, Binary_reader.pk)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  module Public_key_hash = struct
    type t = string
    let compare = String.compare
    let encoding = (Binary_writer.pkh, Binary_reader.pkh)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.signature, Binary_reader.signature)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let to_b58check s = s
end

module Contract_repr = struct
  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.contract, Binary_reader.contract)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let of_b58check s = Ok s
  let to_b58check s = s
end

module Script_expr_hash = struct
  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.script_expr_hash, Binary_reader.script_expr_hash)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = try Some s with _ -> None
  let to_b58check s = s
end

let of_ptime t =
  let (days, ps) = Ptime.Span.to_d_ps (Ptime.to_span t) in
  let s_days = Z.mul (Z.of_int days) (Z.of_int 86_400) in
  Z.add s_days (Z.div (Z.of_int64 ps) (Z.of_int64 1_000_000_000_000L))

let of_notation s = match Ptime.of_rfc3339 s with
  | Ok (t, _, _) ->
    Some (of_ptime t)
  | Error _ ->
    None

module Time = struct
  module Protocol = struct
    let of_notation = of_notation
    let to_seconds x = Z.to_int64 x
  end
end

module Script_timestamp_repr = struct
  type t = Z.t
  let compare = Z.compare
  let to_zint x = x
  let of_zint x = x
  let to_string t = Z.to_string t
  let of_string t = match of_notation t with
    | None -> (try Some (Z.of_string t) with _ -> None)
    | Some z -> Some z
end

module Tez_repr = struct
  type t = int64
  let compare = Int64.compare
  let to_mutez t = t
  let of_mutez t = Some t
  let to_string t = Int64.to_string t
  let of_string t = Int64.of_string_opt t
end

module Ztez_repr = struct
  type t = Z.t
  let compare = Z.compare
  let to_mutez t = t
  let of_mutez t = Some t
  let of_mutez_exn t = t
  let of_tez = Z.of_int64
  let to_tez z = Ok (Z.to_int64 z)
  let to_string t = Z.to_string t
  let of_string t = try Some (Z.of_string t) with _ -> None
end

module Data_encoding = struct
  module Binary = struct
    let to_bytes encoding x =
      try Some ((fst encoding) x) with _ -> None
    let of_bytes encoding b =
      let state = Binary_reader.{ b; offset = 0} in
      try Some (fst ((snd encoding) state)) with _ -> None
  end
end

module Dune_debug = struct
    module Array = Array
end
