open MBytes
open MCrypto

exception Binary_reading_error of string

type state = {b : MBytes.t; offset: int}

let (>>=) (o, s) f = let x, s = f s in (o, x), s

let uint8 s = get_int8 s.b s.offset, {s with offset = s.offset + 1}
let char s = get_char s.b s.offset, {s with offset = s.offset + 1}


let bool s = match char s with
  | '\000', s -> false, s
  | '\255', s -> true, s
  | c, _ -> raise (Binary_reading_error (Printf.sprintf "%c is not a boolean" c))

let opt read_value s = match char s with
  | '\000', s -> None, s
  | '\255', s -> let x, s = read_value s in Some x, s
  | c, _ -> raise (Binary_reading_error (Printf.sprintf "%c is not an option" c))

let int32 s = Int32.to_int (get_int32 s.b s.offset), {s with offset = s.offset + 4}

let uint64 ?(limit=10) s =
  let rec f acc s i =
    if i > limit then raise (Binary_reading_error "int64 too long")
    else (
      let v, s = uint8 s in
      let acc = Int64.(add acc (shift_left (of_int @@ v land 0x7f) (7 * i))) in
      if v land 0x80 <> 0 then f acc s (i+1)
      else acc, s) in
  f 0L s 0

let uzarith ?(limit=10) s =
  let rec f acc s i =
    if i > limit then raise (Binary_reading_error "unsigned zarith too long")
    else
      let v, s = uint8 s in
      let acc =
        Z.add acc (Z.shift_left (Z.of_int @@ v land 0x7f) (7 * i)) in
      if v land 0x80 <> 0 then f acc s (i+1)
      else acc, s in
  f Z.zero s 0

let zarith ?(limit=10) s =
  let rec f sign acc s i =
    if i > limit then raise (Binary_reading_error "signed zarith too long")
    else
      let v, s = uint8 s in
      let sign = if i = 0 then v land 0x40 <> 0 else sign in
      let acc =
        if i = 0 then Z.add acc (Z.of_int @@ v land 0x3f)
        else Z.add acc (Z.shift_left (Z.of_int @@ v land 0x7f) (6 + 7 * (i-1))) in
      if v land 0x80 <> 0 then f sign acc s (i+1)
      else (sign, acc, s) in
  let (sign, acc, s) = f true Z.zero s 0 in
  if sign then Z.neg acc, s else acc, s

let elem f s =
  let len, s = int32 s in
  f len s

let list reader s =
  let f len s =
    let rec aux s l = function
      | 0 -> List.rev l, s
      | i when i > 0 -> let offset = s.offset in
        let x, s = reader s in
        aux s (x :: l) (i - (s.offset - offset))
      | _ -> raise (Binary_reading_error "list longer than expected") in
    aux s [] len in
  elem f s

let sub len s = MBytes.sub s.b s.offset len, {s with offset = s.offset + len}

let pkh s =
  let tag, s = uint8 s in
  let prefix = match tag with
    | 0 -> Prefix.ed25519_public_key_hash
    | 1 -> Prefix.secp256k1_public_key_hash
    | 2 -> Prefix.p256_public_key_hash
    | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for pkh %d" n)) in
  let b, s = sub 20 s in
  Base58.encode prefix b, s

let contract s =
  let tag, s = uint8 s in
  match tag with
  | 0 -> pkh s
  | 1 -> let b, s = sub 20 s in
    Base58.encode Prefix.contract_public_key_hash b, s
  | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for contract %d" n))

let pk s =
  let tag, s = uint8 s in
  let prefix, len = match tag with
    | 0 -> Prefix.ed25519_public_key, 32
    | 1 -> Prefix.secp256k1_public_key, 33
    | 2 -> Prefix.p256_public_key, 33
    | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for pk %d" n)) in
  let b, s = sub len s in
  Base58.encode prefix b, s

let branch s =
  let b, s = sub 32 s in
  Base58.encode Prefix.block_hash b, s

let signature s =
  let b, s = sub 64 s in
  Base58.encode Prefix.generic_signature b, s

let script_expr_hash s =
  let b, s = sub 32 s in
  Base58.encode Prefix.script_expr_hash b, s

let obj1 (a, _s) = a
let obj2 ((a, b), _s) = a, b
let obj3 (((a, b), c), _s) = a, b, c
let obj4 ((((a, b), c), d), _s) = a, b, c, d
let obj5 (((((a, b), c), d), e), _s) = a, b, c, d, e
let obj6 ((((((a, b), c), d), e), f), _s) = a, b, c, d, e, f
let obj7 (((((((a, b), c), d), e), f), g), _s) = a, b, c, d, e, f, g
let obj8 ((((((((a, b), c), d), e), f), g), h), _s) = a, b, c, d, e, f, g, h
let obj9 (((((((((a, b), c), d), e), f), g), h), i), _s) = a, b, c, d, e, f, g, h, i
let obj10 ((((((((((a, b), c), d), e), f), g), h), i), j), _s) = a, b, c, d, e, f, g, h, i, j

let start b = {b; offset = 0}
