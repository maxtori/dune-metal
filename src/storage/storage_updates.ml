open Js_of_ocaml
open Js
open Storage_types

let upgrade_0_to_1 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V0.accounts_entry0 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V1.account_js1 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := array [||];
              a_new) (to_array accounts) in
      let data : V1.accounts_entry1 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let upgrade_1_to_2 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V1.accounts_entry1 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V2.account_js2 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := array [||];
              a_new##.managerKT := undefined ;
              a_new##.admin := array [||] ;
              a_new) (to_array accounts) in
      let data : V2.accounts_entry2 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let upgrade_2_to_3 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V2.accounts_entry2 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V3.account_js3 t = object%js
                val pkh = a_old##.pkh
                val manager = a_old##.manager
                val vault = a_old##.vault
                val name = a_old##.name
                val revealed = array [||]
                val managerKT = undefined
                val admin = array [||]
              end in
              a_new) (to_array accounts) in
      let data : V3.accounts_entry3 t = object%js val accounts = accounts end in
      Chrome.Storage.set Chrome.sync data;
      let config : V3.config_entry3 t = object%js val useDunscan = def _true end in
      Chrome.Storage.set Chrome.sync config
    );
  Chrome.Storage.clear Chrome.Storage.local

let upgrade_3_to_4 () =
  Chrome.Storage.get ~key:"useDunscan" Chrome.sync @@ fun (o:V3.config_entry3 t) ->
  let config : V4.config_entry t = object%js
    val useDunscan = o##.useDunscan
    val theme = def (string "light")
    val batch_notif_ = undefined
    val notif_timeout_ = undefined
  end in
  Chrome.Storage.set Chrome.sync config

let upgrade_4_to_5 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync @@ fun (o:V4.accounts_entry3 t) ->
  let accounts = match Optdef.to_option o##.accounts with None -> [||] | Some accs -> to_array accs in
  let accounts_table = Jstable.create () in
  Array.iteri (fun i a_old ->
      let a_new : V5.account_js t = object%js
        val pkh = a_old##.pkh
        val manager = a_old##.manager
        val vault = a_old##.vault
        val name = a_old##.name
        val revealed = a_old##.revealed
        val managerKT = a_old##.managerKT
        val admin = a_old##.admin
        val index = i
      end in
      Jstable.add accounts_table (string ("account_" ^ string_of_int i)) a_new)
    accounts;
  let keys = array (Array.init (Array.length accounts) (fun i -> i)) in
  let accounts_entry : V5.accounts_entry t = object%js val accounts = def keys end in
  Chrome.Storage.set Chrome.sync accounts_entry;
  Chrome.Storage.set Chrome.sync accounts_table

let downgrade_1_to_0 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V1.accounts_entry1 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V0.account_js0 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := undefined;
              a_new) (to_array accounts) in
      let data : V0.accounts_entry0 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let downgrade_2_to_1 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V2.accounts_entry2 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V1.account_js1 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := a_old##.revealed;
              a_new) (to_array accounts) in
      let data : V1.accounts_entry1 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let downgrade_3_to_2 () =
  Chrome.Storage.get ~key:"accounts" Chrome.sync (fun (o:V3.accounts_entry3 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V2.account_js2 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := array [||];
              a_new##.revealed := array [||];
              a_new) (to_array accounts) in
      let data : V2.accounts_entry2 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let downgrade_4_to_3 () =
  Chrome.Storage.get ~key:"useDunscan" Chrome.sync @@ fun (o:V4.config_entry t) ->
  let config : V3.config_entry3 t = object%js
    val useDunscan = o##.useDunscan
  end in
  Chrome.Storage.set Chrome.sync config

let downgrade_5_to_4 () =
  Chrome.Storage.get Chrome.sync @@ fun (o:V5.accounts_entry t) ->
  let accounts = match Optdef.to_option o##.accounts with None -> [||] | Some accs -> to_array accs in
  let accounts : V4.account_js3 t js_array t = array @@ Array.map (fun i ->
      Unsafe.get o (string ("account_" ^ string_of_int i))) accounts in
  let accounts_entry : V4.accounts_entry3 t = object%js val accounts = def accounts end in
  Chrome.Storage.set Chrome.sync accounts_entry

let upgrades : (int * (unit -> unit)) list = [
  0, upgrade_0_to_1 ;
  1, upgrade_1_to_2 ;
  2, upgrade_2_to_3 ;
  3, upgrade_3_to_4 ;
  4, upgrade_4_to_5 ;
  5, (fun () -> ()) ;
]

let downgrades : (int * (unit -> unit)) list = [
  1, downgrade_1_to_0 ;
  2, downgrade_2_to_1 ;
  3, downgrade_3_to_2 ;
  4, downgrade_4_to_3 ;
  5, downgrade_5_to_4 ;
  6, (fun () -> ()) ;
]

let update_storage () =
  Chrome.Storage.get Chrome.sync (fun (o:version_entry t) ->
      let storage_version = match Optdef.to_option o##.version with
        | None -> 0
        | Some i -> i in
      let direction = compare current_version storage_version in
      let rec aux = function
        | i when i = current_version -> ()
        | i ->
          Chrome.Storage.clear Chrome.Storage.local;
          begin match
              List.assoc_opt i
                (if direction = 1 then upgrades else downgrades) with
          | None -> ()
          | Some update -> update () end;
          let data : version_entry t = Unsafe.obj [||] in
          data##.version := def (i+direction);
          Chrome.Storage.set Chrome.sync data;
          aux (i+direction) in
      aux storage_version)
