(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Ezjs_min
open Metal_types
open Storage_utils

let notified_set ?callback ?data v = match callback with
  | None ->
    Chrome.Storage.set
      ~callback:(fun () -> Notif_background.send ?data ())
      Chrome.sync
      v
  | Some cb ->
    Chrome.Storage.set
      ~callback:(fun () -> cb (); Notif_background.send ?data ())
      Chrome.sync
      v

let set ?callback v =
  Chrome.Storage.set ?callback Chrome.sync v

let get_accounts f =
  Chrome.Storage.get Chrome.sync (fun (o:accounts_entry t) -> f (Of_js.accounts_entry o))

let get_notifs f =
  Chrome.Storage.get Chrome.Storage.local (fun (n : notif_entry t) -> f (Of_js.notifs_entry n))

let update_selected ?callback pkh =
  let data = object%js
    val msg = string "metal account changed"
    val pkh = string @@ MMisc.unopt "none" pkh
  end in
  notified_set ?callback ~data (To_js.selected_entry pkh)

let add_account ?(select=false) ?callback ?accounts account =
  let aux accounts =
    let max_index = List.fold_left (fun ind acc -> max acc.acc_index ind) 0 accounts in
    let account = {account with acc_index = max_index + 1} in
    let b, accounts =
      List.fold_left (fun (b, acc) a ->
          if a.pkh = account.pkh then
            true, { a with vault = account.vault } :: acc
          else false || b, a :: acc)
        (false, []) accounts in
    let accounts =
      if b then List.rev accounts
      else List.rev accounts @ [ account ] in
    let callback = match callback, select with
      | None, false -> None
      | Some callback, select -> Some (fun () ->
          if select then
            update_selected ~callback:(fun () -> callback accounts) (Some account.pkh)
          else
            callback accounts)
      | None, true ->
        Some (fun () -> update_selected (Some account.pkh)) in
    let update_history ?callback () =
      Chrome.Storage.set ?callback Chrome.Storage.local (To_js.accounts_history accounts) in
    let callback () = update_history ?callback () in
    let keys, accounts_table = To_js.accounts_entry accounts in
    set keys;
    let data = object%js
      val msg = string "metal account added"
      val pkh = string account.pkh
    end in
    notified_set ~data ~callback accounts_table in
  match accounts with
  | None -> get_accounts (fun a -> aux (to_listf Of_js.account a))
  | Some accounts -> aux accounts

let remove_account ?callback ?accounts account_pkh =
  let aux accounts =
    let accounts = List.filter (fun {pkh; _} -> pkh <> account_pkh) accounts in
    let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback accounts) in
    let update_history ?callback () =
      Chrome.Storage.set ?callback Chrome.Storage.local (To_js.accounts_history accounts) in
    let callback () = update_history ?callback () in
    let keys, accounts_table = To_js.accounts_entry accounts in
    set keys;
    let data = object%js
      val msg = string "metal account removed"
      val pkh = string account_pkh
    end in
    notified_set ~data ~callback accounts_table in
  match accounts with
  | None -> get_accounts (fun a -> aux (to_listf Of_js.account a))
  | Some accounts -> aux accounts

let update_account ?callback ?accounts account =
  let aux accounts =
    let accounts = List.map (fun a -> if a.pkh = account.pkh then account else a) accounts in
    let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback account accounts) in
    let update_history ?callback () =
      Chrome.Storage.set ?callback Chrome.Storage.local (To_js.accounts_history accounts) in
    let callback () = update_history ?callback () in
    let keys, accounts_table = To_js.accounts_entry accounts in
    set keys;
    let data = object%js
      val msg = string "metal account updated"
      val pkh = string account.pkh
    end in
    notified_set ~data ~callback accounts_table in
  match accounts with
  | None -> get_accounts (fun a -> aux (to_listf Of_js.account a))
  | Some accounts -> aux accounts

let update_network ?callback network =
  let data = object%js
    val msg = string "metal network changed"
    val network = string @@ Mhelpers.network_to_string network
  end in
  notified_set ~data ?callback (To_js.network_entry network)

let update_accounts_js ?callback ?(remove=[]) accounts =
  let accounts_table = Jstable.create () in
  let keys = of_list @@ to_listf (fun acc ->
      Jstable.add accounts_table (string ("account_" ^ (string_of_int acc##.index))) acc;
      acc##.index;
    ) accounts in
  set (object%js val accounts = def keys end : accounts_entry t);
  List.iter (fun i ->
      Chrome.Storage.remove Chrome.sync ("account_" ^ string_of_int i ^ "_")) remove;
  let data = object%js
    val msg = string "metal accounts updated"
  end in
  notified_set ~data ?callback accounts_table

let update_accounts ?callback accounts =
  let keys, accounts = To_js.accounts_entry accounts in
  set keys;
  let data = object%js
    val msg = string "metal accounts updated"
  end in
  notified_set ~data ?callback accounts

let get_approved f =
  Chrome.Storage.get Chrome.sync (fun (o:approved_entry t) ->
      let app = unoptdef_f [||] to_array o##.approved in
      f (Array.to_list app))

let add_approved name callback =
  get_approved (fun old_approved ->
      let approved = name :: old_approved in
      let entry : approved_entry t = Unsafe.obj [||] in
      entry##.approved := def @@ array @@ Array.of_list approved ;
      set ~callback entry)

let get_custom_networks f =
  Chrome.Storage.get Chrome.sync (fun (o:custom_networks_entry t) ->
      f (Of_js.custom_networks_entry o))

let with_custom_networks ?networks f =
  match networks with
  | Some networks -> f networks
  | None -> get_custom_networks f

let update_custom_networks ?callback ?networks new_networks =
  with_custom_networks ?networks @@ fun networks ->
  let new_networks = List.fold_left (fun acc ((name1, _, _) as n) ->
      match List.find_opt (fun (name2, _, _) -> name2 = name1) acc with
      | None -> acc @ [ n ]
      | Some _ -> acc) networks new_networks in
  let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback new_networks) in
  Chrome.Storage.set ?callback Chrome.sync (To_js.custom_networks_entry new_networks)

let add_custom_network ?callback ?networks name node api =
  let network = name, node, api in
  update_custom_networks ?callback ?networks [ network ]

let remove_custom_network ?callback ?networks name =
  with_custom_networks ?networks @@ fun networks ->
  let networks = List.filter (fun (n, _, _) -> n <> name) networks in
  let callback = match callback with
    | None -> None
    | Some callback -> Some (fun () -> callback networks) in
  Chrome.Storage.set ?callback Chrome.sync (To_js.custom_networks_entry networks)

let remove_notif ?callback n =
  let id = Mhelpers.notif_id_of_not n in
  get_notifs (fun ns ->
      let ns = List.map (fun {notif_acc ; notifs} ->
          { notif_acc ;
            notifs =
              List.filter (fun op -> Mhelpers.notif_id_of_not op <> id) notifs
          }
        ) ns in
      let callback = MMisc.convopt (fun cb -> (fun () -> cb ns)) callback in
      Chrome.Storage.set ?callback Chrome.Storage.local @@ To_js.notifs_entry ns)

let add_notif ?callback acc notif =
  get_notifs (fun ns ->
      let ok, notifs =
        List.fold_left (fun (ok, n) { notif_acc ; notifs } ->
            if notif_acc = acc then
              true, { notif_acc ; notifs = notif :: notifs } :: n
            else ok, { notif_acc ; notifs } :: n
          ) (false, []) ns in
      let notifs =
        if ok then To_js.notifs_entry notifs
        else To_js.notifs_entry @@ { notif_acc = acc ; notifs = [ notif ] } :: notifs in
      Chrome.Storage.set ?callback Chrome.Storage.local notifs)

let clear_local () =
  Chrome.Storage.clear Chrome.Storage.local

let reset_storage ?callback () =
  Chrome.Storage.clear Chrome.sync;
  clear_local ();
  match callback with
  | None -> ()
  | Some cb -> cb ()

let update_config ?callback config =
  let callback = match callback with
    | None -> None
    | Some cb -> Some (fun () -> cb config) in
  Chrome.Storage.set ?callback Chrome.sync (To_js.config config)

let update_pendings ?callback network pending account =
  update_account ?callback
    {account with
     pending = Mhelpers.update_network_assoc network account.pending pending}

let add_pending ?callback network pending bo account =
  let pending = bo :: (Mhelpers.get_network_assoc [] network pending) in
  update_pendings ?callback network pending account
