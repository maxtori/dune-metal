open Hacl
open Secretbox

let salt_len = 8
let nonce = Bigstring.make Nonce.bytes '\x00'

let pbkdf ~salt ~password =
  Pbkdf.pbkdf2 ~count:32768 ~dk_len:32l ~salt ~password

let box ~key ~sk =
  let cmsg = Bigstring.concat "" [Bigstring.make zerobytes '\x00'; sk] in
  box ~key ~nonce ~msg:cmsg ~cmsg;
  Bigstring.sub cmsg boxzerobytes (Bigstring.length cmsg - boxzerobytes)

let encrypt ~password sk =
  let salt = Hacl.Rand.gen salt_len in
  let key = unsafe_of_bytes @@ pbkdf ~salt ~password in
  Bigstring.concat "" [ salt; box ~key ~sk ]

let box_open ~key ~esk =
  let msg = Bigstring.concat "" [Bigstring.make boxzerobytes '\x00'; esk] in
  match box_open ~key ~nonce ~cmsg:msg ~msg with
  | false -> None
  | true -> Some (Bigstring.sub msg zerobytes (Bigstring.length esk - boxzerobytes))

let decrypt ~password esk =
  let salt = Bigstring.sub esk 0 salt_len in
  let esk = Bigstring.sub esk salt_len (boxzerobytes + 32) in
  let key = unsafe_of_bytes (pbkdf ~salt ~password) in
  box_open ~key ~esk
