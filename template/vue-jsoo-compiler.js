const compiler = require('vue-template-compiler')
const utils = require('@vue/component-compiler-utils')
const fs = require('fs')

var output = process.argv[2]
var names = process.argv.slice(3)
var s_js = ''
var s_mli = 'open Js_of_ocaml.Js\n'
for (name of names) {
  const source = fs.readFileSync(name + '.html', 'utf8')
  let c = utils.compileTemplate({
    source,
    compiler,
    isProduction : true,
    compilerOptions : { whitespace : 'condense' }
  })
  c = c.code.replace('var render = function ()', '//Provides: ' + name + '_render\nfunction '+ name + '_render(createElement)')
  c = c.replace('var staticRenderFns', '\n//Provides: ' + name + '_static_renders\nvar ' + name + '_static_renders')
  c = c.replace(/function \(\)/g, 'function(createElement)')
  s_js += c.replace(/_vm\.\$createElement/g, 'createElement') + '\n'
  s_mli += 'let ' + name + '_render = Unsafe.variable "' + name +'_render"\nlet ' + name + '_static_renders = Unsafe.variable "' + name +'_static_renders"\n'
}
fs.writeFile(output + '.js', s_js , function() {})
fs.writeFile(output + '.ml', s_mli , function() {})
