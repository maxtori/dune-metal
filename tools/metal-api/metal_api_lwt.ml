include Metal_api.Metal
module Js = Metal_api.Js

let get_account ?m () =
  let (promise, resolver) = Lwt.task () in
  get_account ?m (Lwt.wakeup resolver);
  promise

let get_network ?m () =
  let (promise, resolver) = Lwt.task () in
  get_network ?m (Lwt.wakeup resolver);
  promise

let send ?m ?fee ?parameter ?entrypoint ?gas_limit ?storage_limit ?collect_call ?network ?typ
    ~destination amount =
   let (promise, resolver) = Lwt.task () in
   send ?m ~destination ~amount
     ?fee ?parameter ?entrypoint ?gas_limit ?storage_limit ?collect_call
     ?network ?typ (Lwt.wakeup resolver);
   promise

let originate ?m ?code
    ?fee ?gas_limit ?storage_limit ?network ?typ balance =
  let (promise, resolver) = Lwt.task () in
  originate ?m ~balance ?code
    ?fee ?gas_limit ?storage_limit ?network ?typ (Lwt.wakeup resolver);
  promise

let delegate ?m ?fee ?gas_limit ?storage_limit ?network ?typ dlg =
   let (promise, resolver) = Lwt.task () in
   delegate ?m
     ?fee ?gas_limit ?storage_limit ?network ?typ dlg (Lwt.wakeup resolver);
   promise

let batch ?m ?network ?typ operations =
  let (promise, resolver) = Lwt.task () in
  batch ?m ?network ?typ operations (Lwt.wakeup resolver);
  promise
