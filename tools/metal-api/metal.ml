open Ezjs_min
open Metal_js

let is_installed = ref false
let metal m : metal t = match m with
  | Some metal -> metal
  | None -> match Optdef.to_option (Unsafe.variable "metal") with
    | None -> failwith "metal is not installed yet"
    | Some o -> is_installed := true; o

type operation_type = Transaction | Origination | Delegation

let ready ?not_installed ?timeout f =
  ready ?not_installed ?timeout (fun m -> f m)

let installed () = !is_installed

let is_enabled ?m f =
  (metal m)##isEnabled(fun js_b -> f (to_bool js_b))

let is_unlocked ?m f =
  (metal m)##isUnlocked(fun js_b -> f (to_bool js_b))

let is_approved ?m f =
  (metal m)##isApproved(fun js_b -> f (to_bool js_b))

let get_account ?m f =
  (metal m)##getAccount(fun js_str -> f (to_string js_str))

let get_network ?m f =
  (metal m)##getNetwork(fun res ->
      f (to_string res##.name, to_string res##.url))

let on_state_changed ?m f =
  (metal m)##onStateChanged f

type 'a result =
  | Ok of 'a
  | Canceled

type code =
  | Nocode
  | Code of string * string
  | CodeHash of string * string

let map_opt f = function
  | None -> undefined
  | Some o -> def (f o)

let op_callback callback res =
  match to_bool res##.ok with
  | false -> callback Canceled
  | true ->
    callback
      (Ok (to_string (Optdef.get (res##.msg) (fun () -> assert false))))

let send_callback callback (res : send_result t) =
  match to_optdef to_bool res##.ok with
  | Some false -> callback Canceled
  | Some true ->
    callback
      (Ok (to_string (Optdef.get (res##.msg) (fun () -> assert false))))
  | None ->
    callback (Ok (to_string (Unsafe.coerce res)))

let make_transaction ~destination ~amount ?fee ?parameter ?entrypoint
    ?gas_limit ?storage_limit ?collect_call ?network ?typ callback : send_param t  =
  let destination = string destination in
  let amount = string amount in
  let fee = map_opt string fee in
  let parameter = map_opt string parameter in
  let entrypoint = map_opt string entrypoint in
  let gas_limit = map_opt (fun x -> string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> string (string_of_int x)) storage_limit in
  let collect_call = map_opt bool collect_call in
  let callback = send_callback callback in
  object%js
    val dst = destination
    val amount = amount
    val fee = fee
    val parameter = parameter
    val entrypoint = entrypoint
    val gas_limit_ = gas_limit
    val storage_limit_ = storage_limit
    val collect_call_ = collect_call
    val network = map_opt string network
    val type_ = map_opt string typ
    val cb = def (wrap_callback callback)
  end

let send ?m ~destination ~amount
    ?fee ?parameter ?entrypoint ?gas_limit ?storage_limit ?collect_call ?network ?typ callback =
  let data = make_transaction ~destination ~amount
      ?fee ?parameter ?entrypoint ?gas_limit ?storage_limit ?collect_call ?network ?typ callback in
  (metal m)##send data

let make_origination ~balance ?(code=Nocode) ?fee ?gas_limit ?storage_limit
    ?network ?typ callback : originate_param t =
  let balance = string balance in
  let fee = map_opt string fee in
  let gas_limit = map_opt (fun x -> string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> string (string_of_int x)) storage_limit in
  let sc_storage, sc_code, sc_code_hash = match code with
    | Nocode -> Optdef.empty, Optdef.empty, Optdef.empty
    | Code (code, storage) ->
        Optdef.return (string storage),
        Optdef.return (string code),
        Optdef.empty
    | CodeHash (code_hash, storage) ->
        Optdef.return (string storage),
        Optdef.empty,
        Optdef.return (string code_hash)
  in
  let callback = fun res ->
    match to_optdef to_bool res##.ok with
    | Some false | None -> callback Canceled
    | Some true ->
        let op_hash = to_string res##.op_hash in
        let contract = to_string res##.contract in
        callback (Ok (op_hash, contract))
  in
  (object%js
    val balance = balance
    val fee = fee
    val gas_limit_ = gas_limit
    val storage_limit_ = storage_limit
    val sc_storage_ = sc_storage
    val sc_code_ = sc_code
    val sc_code_hash_ = sc_code_hash
    val network = map_opt string network
    val type_ = map_opt string typ
    val cb = def (wrap_callback callback)
  end)

let originate ?m ~balance ?code ?fee ?gas_limit ?storage_limit ?network ?typ callback =
  let data =
    make_origination ~balance ?code ?fee ?gas_limit ?storage_limit ?network ?typ callback  in
  (metal m)##originate data

let make_delegation ?fee ?gas_limit ?storage_limit ?network ?typ delegate callback : delegate_param t =
  let delegate = match delegate with None -> undefined | Some d -> def (string d) in
  let fee = map_opt string fee in
  let gas_limit = map_opt (fun x -> string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> string (string_of_int x)) storage_limit in
  let callback = send_callback callback in
  (object%js
    val delegate = delegate
    val fee = fee
    val gas_limit_ = gas_limit
    val storage_limit_ = storage_limit
    val network = map_opt string network
    val type_ = map_opt string typ
    val cb = def (wrap_callback callback)
  end)

let delegate ?m ?fee ?gas_limit ?storage_limit ?network ?typ delegate callback =
  let data = make_delegation ?fee ?gas_limit ?storage_limit ?network ?typ delegate callback in
  (metal m)##delegate data

let make_batch_operation ?fee ?gas_limit ?storage_limit ?amount
    ?destination ?parameter ?entrypoint ?balance ?code ?delegate kind =
  let o : batch_operation t = Unsafe.obj [||] in
  begin match kind, amount, destination, balance, delegate with
    | Transaction, Some amount, Some destination, _, _ ->
      let tr = make_transaction ~destination ~amount ?fee ?parameter ?entrypoint
          ?gas_limit ?storage_limit (fun _ -> ()) in
      o##.kind := string "transaction";
      o##.operation_transaction := def tr
    | Origination, _, _, Some balance, _ ->
      let ori = make_origination ~balance ?code
          ?fee ?gas_limit ?storage_limit (fun _ -> ()) in
      o##.kind := string "origination";
      o##.operation_origination := def ori
    | Delegation, _, _, _, Some delegate ->
      let del = make_delegation ?fee ?gas_limit ?storage_limit delegate (fun _ -> ()) in
      o##.kind := string "delegation";
      o##.operation_delegation := def del
    | _ -> assert false
  end;
  o

let batch ?m ?network ?typ operations callback =
  let callback = send_callback callback in
  (metal m)##batch (object%js
    val operations = operations
    val network = map_opt string network
    val type_ = map_opt string typ
    val cb = def (wrap_callback callback)
  end)
